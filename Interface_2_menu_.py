from Criando um Menu.lib.interface import *
from Criando um Menu.lib.arquivo import *
from time import sleep

arq = 'Cadastro funcionarios.txt'


if not arquivoExiste(arq):
  criarArquivo(arq)


while True:
  resposta = menu(['Ver pessoas cadastradas', 'Cadastrar nova pessoa', 'Sair do sistema'])
  if resposta == 1:
    print('Opção 1')
    #opção de listar o conteudo de um arquivo
    lerArquivo(arq)
  elif resposta == 2:
    #opçao de cadastrar uma nova pessoa
    cabeçlaho('Novo Cadastro: ')
    nome = str(input('Nome: '))
    idade = leiaInt('Idade: ')
    cadastrar(arq, nome, idade)
  elif resposta == 3:
    #opção de sair do sistema
    print('Saindo do sistema... Até logo')
    break 
  else:
    print('\033[31mErro! Digite uma opção valida\033[m')
  sleep(2)
  